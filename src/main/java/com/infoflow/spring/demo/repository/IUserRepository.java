package com.infoflow.spring.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.infoflow.spring.demo.entity.User;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

}
