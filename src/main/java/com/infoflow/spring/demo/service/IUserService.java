package com.infoflow.spring.demo.service;

import java.util.List;

import com.infoflow.spring.demo.entity.User;

public interface IUserService {
	
	User findById(String id);
	void save(User user);
	List<User> findall();

}
