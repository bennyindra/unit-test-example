package com.infoflow.spring.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.infoflow.spring.demo.entity.User;
import com.infoflow.spring.demo.service.IUserService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/rest")
@Api(value = "User Post Demo Controller", tags = "User Post Demo Controller") 
public class UserPostController {
	
	@Autowired
	IUserService userService;

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, value = "/save/user")
	public ResponseEntity<String> matchingCrn(@RequestBody User request,
			BindingResult bindingResult, HttpServletRequest httpServletRequest, @RequestHeader HttpHeaders headers) {
		userService.save(request);
		return ResponseEntity.ok().body(request.getId());
	}
	
}
