package com.infoflow.spring.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.infoflow.spring.demo.entity.User;
import com.infoflow.spring.demo.service.IUserService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/rest")
@Api(value = "User Get Demo Controller", tags = "User Get Demo Controller")
public class UserGetController {

	@Autowired
	IUserService userService;
	
	@RequestMapping(method=RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}/id")
	public ResponseEntity<User> getUser(@PathVariable String id) {
		return ResponseEntity.ok(userService.findById(id));
	}
	
	@RequestMapping(method=RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE, value = "/findall")
	public ResponseEntity<List<User>> getAllUser() {
		return ResponseEntity.ok(userService.findall());
	}
	
	@RequestMapping(method=RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE, value = "/generateData")
	@ResponseStatus(code=HttpStatus.OK)
	public void generateData() {
		for (Integer i = 0; i < 10; i++) {
			User user = new User();
			user.setId(UUID.randomUUID().toString());
			user.setBirthDay(new Date());
			user.setName("BUDI"+i);
			userService.save(user);
		}
	}
}
