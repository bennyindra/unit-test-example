package com.infoflow.spring.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.infoflow.spring.demo.MainTests;
import com.infoflow.spring.demo.entity.User;
import com.infoflow.spring.demo.repository.IUserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainTests.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(locations = "classpath:application.properties")
@ActiveProfiles("test")
public class UserServiceTest {

	@InjectMocks
	UserService userService;

	@Mock
	IUserRepository userRepository;

	@Test
	public void save() {
		User user = new User();
		user.setId("xx");
		Mockito.doReturn(user).when(userRepository).save(Mockito.any(User.class));
		userService.save(user);
	}

	@Test
	public void findall() {
		List<User> resultMock = new ArrayList<>();
		for (Integer i = 0; i <10 ; i++) {
			User user = new User();
			user.setId(i.toString());
			user.setName(i.toString()+"xx");
			user.setBirthDay(new Date());
			resultMock.add(user);
		}
		Mockito.doReturn(resultMock).when(userRepository).findAll();
		List<User> list = userService.findall();
		for (User user : list) {
			Assert.assertTrue(user.getName().endsWith("xx"));
		}
	}
	
	@Test
	public void findallWithErrorNPE() {
		Mockito.doThrow(NullPointerException.class).when(userRepository).findAll();
		try {
			userService.findall();
		} catch (Exception e) {
			Assert.assertTrue(e instanceof NullPointerException);
		}
	}

}
