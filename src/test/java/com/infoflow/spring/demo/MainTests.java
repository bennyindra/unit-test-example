package com.infoflow.spring.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.infoflow.spring.demo"})
public class MainTests {

	
	public static void main(String[] args) {
		SpringApplication.run(MainTests.class, args);
	}
}
