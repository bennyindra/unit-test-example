package com.infoflow.spring.demo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infoflow.spring.demo.MainTests;
import com.infoflow.spring.demo.entity.User;
import com.infoflow.spring.demo.service.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainTests.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(locations="classpath:application.properties")
public class UserPostControllerTest {

	protected MockMvc mockMvc;
	@Autowired
	protected WebApplicationContext wac;
	
	@MockBean
	private IUserService userService;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void testSuccess() throws Exception {
		User user  = new User();
		user.setBirthDay(new Date());
		user.setId("aa");
		user.setName("yanu");
		
		Mockito.doNothing().when(userService).save(Mockito.any());
		
		mockMvc.perform(post("/rest/save/user").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(user))
		.header("header1","aaa"))
		.andExpect(new ResultMatcher() {
			
			@Override
			public void match(MvcResult result) throws Exception {
				Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
			}
		});
	}

}
