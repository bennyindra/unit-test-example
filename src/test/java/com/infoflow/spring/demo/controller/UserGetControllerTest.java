package com.infoflow.spring.demo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.h2.value.NullableKeyConcurrentMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infoflow.spring.demo.MainTests;
import com.infoflow.spring.demo.entity.User;
import com.infoflow.spring.demo.service.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainTests.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(locations="classpath:application.properties")
public class UserGetControllerTest {
	protected MockMvc mockMvc;
	@Autowired
	protected WebApplicationContext wac;
	
	@MockBean
	private IUserService userService;
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void test() throws Exception {
		List<User> users = new ArrayList<>();
		for (Integer i = 0; i < 10; i++) {
			User user = new User();
			user.setId(UUID.randomUUID().toString());
			user.setBirthDay(new Date());
			user.setName("BUDI"+i);
			users.add(user);
		}
		Mockito.when(userService.findall()).thenReturn(users);
		mockMvc.perform(get("/rest/findall")).andExpect(new ResultMatcher() {
			
			@Override
			public void match(MvcResult result) throws Exception {
				List<User> list = new ObjectMapper().readValue(result.getResponse().getContentAsString(), List.class);
				Assert.assertEquals(10, list.size());
			}
		});
	}
}
